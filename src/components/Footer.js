import React from 'react'

const Footer = () => {
    let w = 150
    return (
        <footer id='contact'>
            <p>You may contact me at <a href='mailto:saad.shaikh.dev@protonmail.com'>saad.shaikh.dev@protonmail.com</a>.</p>
            <div className='external-links'>
                <a href='https://gitlab.com/saad.shaikh/' target="_blank" rel="noreferrer">
                    <img src='https://about.gitlab.com/images/press/logo/png/gitlab-logo-100.png' width={w} alt="Gitlab"></img>
                </a>
                <a href='https://www.linkedin.com/in/saad-shaikh-dev/' target="_blank" rel="noreferrer">
                    <img src='https://upload.wikimedia.org/wikipedia/commons/0/01/LinkedIn_Logo.svg' width={w} alt="LinkedIn"></img>
                </a>
                <a href='https://medium.com/@saad.shaikh' target="_blank" rel="noreferrer">
                    <img src='https://upload.wikimedia.org/wikipedia/commons/0/0d/Medium_%28website%29_logo.svg' width={w} alt="Medium"></img>
                </a>
                <a href='https://stackoverflow.com/users/14394673/saad-shaikh' target="_blank" rel="noreferrer">
                    <img src='https://upload.wikimedia.org/wikipedia/commons/0/02/Stack_Overflow_logo.svg' width={w} alt="Stack Overflow"></img>
                </a>
            </div>
            <p>Copyright 2022 - Saad Shaikh</p>
        </footer>
    )
}

export default Footer