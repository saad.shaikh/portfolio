import React from 'react'
import { Link } from 'gatsby'
import { useLocation } from '@reach/router';

const Header = () => {
    const pageLinks = [
        { id: 1, link: "/", name: "Home" },
        { id: 2, link: "/projects", name: "Projects" },
        { id: 3, link: "/articles", name: "Articles" },
        { id: 4, link: "#contact", name: "Contact" }
    ]

    let currentLocation = useLocation()

    return (
        <header>
            <Link to="/"><h1>Saad Shaikh</h1></Link>
            <nav>
                {pageLinks.map(pageLink => {
                    return (
                        <Link
                            key={pageLink.id}
                            to={pageLink.link}
                            className={pageLink.link === currentLocation.pathname ? "current-page" : ""}
                        >
                            {pageLink.name}
                        </Link>
                    )
                })}
            </nav>
        </header>
    )
}

export default Header