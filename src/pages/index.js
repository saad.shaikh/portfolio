import React, { Fragment } from "react"
import { Helmet } from "react-helmet"
import "../all.css"
import { titleEnding } from "../all"
import Layout from "../components/Layout"

const IndexPage = () => {
  const pageName = "Home"

  let stack = [
    {
      id: 1,
      src: "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6a/JavaScript-logo.png/600px-JavaScript-logo.png?20120221235433",
      alt: "javascript"
    },
    {
      id: 2,
      src: "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Typescript_logo_2020.svg/512px-Typescript_logo_2020.svg.png?20210506173343",
      alt: "typescript"
    },
    {
      id: 3,
      src: "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/512px-React-icon.svg.png?20220125121207",
      alt: "react"
    },
    {
      id: 4,
      src: "https://upload.wikimedia.org/wikipedia/en/thumb/d/d0/Gatsby_Logo.png/220px-Gatsby_Logo.png",
      alt: "gatsby"
    },
    {
      id: 5,
      src: "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8e/Nextjs-logo.svg/207px-Nextjs-logo.svg.png?20190307203525",
      alt: "next.js"
    },
    {
      id: 6,
      src: "https://upload.wikimedia.org/wikipedia/commons/thumb/9/95/Vue.js_Logo_2.svg/512px-Vue.js_Logo_2.svg.png?20170919082558",
      alt: "vue.js"
    },
    {
      id: 7,
      src: "https://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/Svelte_Logo.svg/498px-Svelte_Logo.svg.png?20191219133350",
      alt: "svelte"
    },
    { id: 8, src: "https://vitejs.dev/logo.svg", alt: "vite" }
  ]

  const randomize = (arr) => {
    let arrClone = arr
    let result = []
    while (arrClone.length > 0) {
      let randomNumber = Math.floor(Math.random() * arrClone.length)
      result.push(arrClone[randomNumber])
      arrClone.splice(randomNumber, 1)
    }
    return result
  }

  stack = randomize(stack)


  return (
    <Fragment>
      <Helmet title={pageName + titleEnding} />
      <Layout>
        <div className="home">
          <h2>Frontend Developer</h2>
          <section className="hero">
            {stack && stack.map(i => <img src={i.src} alt={i.alt} key={i.id}></img>)}
          </section>
          <p>Hi, My name is Saad and I am a Frontend Web Developer.</p>
          <p>I am proficient in <span className="react">React</span> (including <span className="gatsby">Gatsby</span> and <span className="next-js">Next.js</span>), <span className="vue-js">Vue.js</span>, <span className="svelte">Svelte</span> & <span className="vite">Vite</span> and love using pure CSS and creating aesthetically pleasing designs from scratch and coming up with solutions to design architectural problems.</p>
          <p className="para-last">Feel Free to check out my projects and articles.</p>
        </div>
      </Layout>
    </Fragment>
  )
}; export default IndexPage