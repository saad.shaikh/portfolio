import React, { Fragment, useState, useEffect } from "react"
import { Helmet } from "react-helmet"
import "../all.css"
import { titleEnding } from "../all.js"
import Layout from "../components/Layout"
import fetch from 'node-fetch';

const Projects = () => {

    let pageName = "My Projects"

    const [search, setSearch] = useState("")
    const [projects, setProjects] = useState()

    const getProjects = async () => {
        const requestOptions = {
            method: 'GET',
            headers: {
                "Access-Control-Request-Headers": process.env.GATSBY_PROJECTS_ACCESS_CONTROL,
                "Authorization": process.env.GATSBY_PROJECTS_BEARER
            },
            redirect: 'follow'
        };
        try {
            let response = await fetch(
                process.env.GATSBY_PROJECTS_API + search, requestOptions);
            let result = await response.json();
            setProjects(result)
        } catch (err) { console.error(err); }
    };

    const searchChange = (e) => setSearch(e.target.value.toString())

    const masonryLayout = () => {
        let projectElements = document.getElementById("projects").querySelectorAll(".project")
        const adjustVerticalPosition = (columns) => {
            for (let i = columns; i <= (Object.values(projects).length - 1); i++) {
                const aboveCardTop = projectElements[i - columns].offsetTop
                const aboveCardHeight = projectElements[i - columns].offsetHeight
                const currentCardTop = projectElements[i].offsetTop
                const currentGap = currentCardTop - (aboveCardTop + aboveCardHeight)
                projectElements[i].style = `top: calc(-${currentGap}px + 1.5em);`
            }

            if (projectElements.length > 0) {
                const parentTop = document.getElementById("projects").offsetTop
                const parentHeight = document.getElementById("projects").offsetHeight
                const parentCurrentBottom = parentTop + parentHeight

                let newParentBottom = 0
                let iterNumber = 1
                while (iterNumber <= projectElements.length && iterNumber <= 3) {
                    const lastTop = projectElements[Object.values(projects).length - iterNumber].offsetTop
                    const lastHeight = projectElements[Object.values(projects).length - iterNumber].offsetHeight
                    if (newParentBottom < (lastTop + lastHeight)) newParentBottom = lastTop + lastHeight
                    iterNumber++
                }
                document.getElementById("projects").style = `margin-bottom:-${parentCurrentBottom - newParentBottom}px;`
            }
        }
        for (let i = 0; i <= (Object.values(projects).length - 1); i++) {
            projectElements[i].style = ""
        }
        if (window.innerWidth >= 1000 * 100 / 90) adjustVerticalPosition(3)
        else if (window.innerWidth >= 1000 * 2 / 3 * 10 / 9) adjustVerticalPosition(2)
    }

    useEffect(() => {
        getProjects()
        search.length > 0 && search.length < 3 ?
            document.getElementById("search-tip").style.display = "block"
            : document.getElementById("search-tip").style.display = ""
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [search])

    useEffect(() => {
        projects && masonryLayout()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [projects])

    useEffect(() => {
        if (typeof window === 'undefined') return;
        window.addEventListener('resize', getProjects);
        return () => {
            window.removeEventListener('resize', getProjects)
        }
    });

    return (
        <Fragment>
            <Helmet title={pageName + titleEnding} />
            <Layout>
                <h2>My Projects</h2>
                <input
                    className="search"
                    placeholder="Search through my projects.."
                    onChange={searchChange}></input>
                <span id="search-tip" role="tooltip">Enter at least 3 characters to search</span>
                {projects && <p>Showing {projects.length} projects</p>}
                <div id="projects" className="projects">
                    {projects ? Object.values(projects).map(project => {
                        return (
                            <div
                                id={"project-" + project.id}
                                className="project"
                                key={project.id}
                            >
                                <img
                                    src={project.avatar_url ?
                                        project.avatar_url :
                                        "https://upload.wikimedia.org/wikipedia/commons/e/e1/GitLab_logo.svg"}
                                    className={!project.avatar_url ? "image-contain" : ""}
                                    alt="">
                                </img>
                                <div className="project-text">
                                    <h3>{project.name.toUpperCase()}</h3>
                                    <p>
                                        <strong>Created on:</strong> &nbsp;&nbsp;&nbsp;
                                        {project.created_at.substr(8, 2)}
                                        -
                                        {project.created_at.substr(5, 2)}
                                        -
                                        {project.created_at.substr(0, 4)}
                                    </p>
                                    <p>
                                        <strong>Last modified:</strong>&nbsp;
                                        {project.last_activity_at.substr(8, 2)}
                                        -
                                        {project.last_activity_at.substr(5, 2)}
                                        -
                                        {project.last_activity_at.substr(0, 4)}
                                    </p>
                                    {project.description &&
                                        <div className="desc">
                                            {project.description.startsWith("Tech stack: ") &&
                                                <Fragment>
                                                    <strong>Tech stack:</strong>
                                                    <br></br>
                                                </Fragment>
                                            }
                                            {project.description.replace("Tech stack: ", "").split("\r\n").map(i => <p>{i}</p>)}
                                        </div>

                                    }
                                    <a className="list-item-link" href={project.web_url} target="_blank" rel="noreferrer">
                                        Go to repository &rarr;
                                    </a>
                                </div>
                            </div>
                        )
                    }) : "Loading.."}
                </div>
            </Layout>
        </Fragment >
    )
}; export default Projects