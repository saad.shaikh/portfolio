import React, { Fragment, useState } from "react"
import { Helmet } from "react-helmet"
import "../all.css"
import { titleEnding } from "../all"
import Layout from "../components/Layout"
import { articles } from "../../articles.js"

const Articles = () => {

    let pageName = "My Articles"

    const [search, setSearch] = useState("")
    const searchChange = (e) => {
        setSearch(e.target.value.toString())
    }

    return (
        <Fragment>
            <Helmet title={pageName + titleEnding} />
            <Layout>
                <h2>My Articles</h2>
                <input
                    className="search"
                    placeholder="Search through my articles.."
                    onChange={searchChange}></input>
                <span id="search-tip" role="tooltip">Enter at least 3 characters to search</span>
                {articles &&
                    <p>
                        Showing {articles.filter(i => i.title.toLocaleLowerCase().includes(search)).length} articles
                    </p>}
                <div className="articles">
                    {articles ? Object.values(articles).filter(i => i.title.toLocaleLowerCase().includes(search)).map(article => {
                        return (
                            <div id={"article-" + article.id} className="article" key={article.id}>
                                <h2>{article.title.toUpperCase()}</h2>
                                <small className="article-date">Date published: {article.published_on}</small>
                                <p><i>{article.brief}</i></p>
                                <a className="list-item-link" href={article.link} target="_blank" rel="noreferrer">
                                    Read more &rarr;
                                </a>
                            </div>
                        )
                    }) : "Loading.."}
                </div>
            </Layout>
        </Fragment>
    )
}; export default Articles